/* =====================================
Template Name: TravelTrek
Author Name: iThemer
Author URI: http://ithemer.com/
Description: TravelTrek is a Travel Agency & Tour Booking HTML5 Template.
Version:	1.0
========================================*/
/*=======================================
[Start Activation Code]
=========================================
	01. Mobile Menu JS
	02. Mobile Menu JS
=========================================
[End Activation Code]
=========================================*/ 
(function($) {
    "use strict";
     $(document).on('ready', function() {	
		
		/*====================================
		01. Sticky Header JS
		======================================*/ 
		jQuery(window).on('scroll', function() {
			if ($(this).scrollTop() > 150) {
				$('.site-header').addClass("sticky");
			} else {
				$('.site-header').removeClass("sticky");
			}
		});
		
			
		
		/*====================================
		02. Mobile Menu
		======================================*/ 	
		$('.menu').slicknav({
			prependTo:".mobile-nav",
			duration: 600,
			closeOnClick:true,
		});
		
		
		$('.single-faq .faq-title').on('click', function() {
            $(".single-faq .faq-title").removeClass("active");
            $(this).addClass("active");
		});
	});
		/*=====================================
		18. Others JS
		======================================*/ 	
		$( function() {
			$( "#slider-range" ).slider({
			  range: true,
			  min: 0,
			  max: 500,
			  values: [ 0, 500 ],
			  slide: function( event, ui ) {
				$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			  }
			});
			$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			  " - $" + $( "#slider-range" ).slider( "values", 1 ) );
		} );
		
		$(window).on('load', function() {
				$('.cp-preloader').fadeOut('slow', function(){
				$(this).remove();
			});
		});
})(jQuery);
