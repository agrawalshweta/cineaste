<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Events;
use App\Entity\Contact;
use App\Repository\ContactRepository;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact_index")
     *
     */
    public function index()
    {
        return $this->render('contact/index.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
        ]);
    }

    /**
     * @Route("/save_contact", name="save_contact")
     *
     */
    public function saveContact(Request $request)
    {
        $input = $request->request->all();

            $validator = Validation::createValidator();

            $constraint = new Assert\Collection([
                '_full_name'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Name should not be blank.']),
                '_email' => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Email should not be blank.']),
                '_subject' => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Subject should not be blank.']),
                '_message' => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Message should not be blank.']),
                '_captcha'    => new \Symfony\Component\Validator\Constraints\Optional(),
                
            ]);

        if ('POST' == $request->getMethod()) {

            $violations = $validator->validate($input, $constraint);

            if (count($violations) == 0) {
                $fullName = $request->request->get('_full_name');
                $email = $request->request->get('_email'); 
                $subject = $request->request->get('_subject');
                $message = $request->request->get('_message');

                $contact = new Contact();

                $contact->setFullName($fullName);
                $contact->setEmail($email);
                $contact->setSubject($subject);
                $contact->setMessage($message);
                $contact->setPublishedAt(new \DateTime());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($contact);
                $entityManager->flush();

                return $this->redirectToRoute('contact_index');
            } else {
                foreach ($violations as $error) {
                   
                    $field = substr($error->getPropertyPath(), 1, -1);

                    $errors[$field] = $error->getMessage();
                }

                return $this->render('contact/index.html.twig', [
                    'last_username' => '',
                    'otherError' => $errors,
                    'error' => '',
                ]);
            }
        }

        return $this->render('contact/index.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
        ]);
    }

}
