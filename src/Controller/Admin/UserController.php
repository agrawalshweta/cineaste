<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Events;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/admin/users_list", name="users_list")
     *
     */
    public function index(UserRepository $user)
    {
        
        $userDetail = $user->findBy([], ['publishedAt' => 'DESC']);

        return $this->render('admin/user/index.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'users' => $userDetail
        ]);
    }

    /**
     * @Route("/admin/user_edit/{id<\d+>}", name="edit_user")
     *
     */
    public function editUser(User $usr)
    {
        return $this->render('admin/user/user_edit.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'single_user'=>$usr
        ]);
    }

    /**
     * Update User.
     *
     * @Route("/admin/update_user", name="update_user")
     */
    public function updateCat(Request $request, UserPasswordEncoderInterface $encoder)
    {
            $input = $request->request->all();
            
            $validator = Validation::createValidator();

            $constraint = new Assert\Collection([
                '_id'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'ID should not be blank.']),
                '_first_name'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'First name should not be blank.']),
                '_full_name'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Name should not be blank.']),
                '_pass'    => new \Symfony\Component\Validator\Constraints\Optional(),
                '_profession'    => new \Symfony\Component\Validator\Constraints\Optional(),
                '_department'    => new \Symfony\Component\Validator\Constraints\Optional(),
                '_company'    => new \Symfony\Component\Validator\Constraints\Optional(),
                '_site_url'    => new \Symfony\Component\Validator\Constraints\Optional(),
                
             ]);

            $id = $request->request->get('_id');
            $password = $request->request->get('_password');
            $firstName = $request->request->get('_first_name');
            $fullName = $request->request->get('_full_name');
            $profession = $request->request->get('_profession');
            $department = $request->request->get('_department');
            $company = $request->request->get('_company');
            $site_url = $request->request->get('_site_url');

            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)->find($id);

           
            if (!$user) {
                throw $this->createNotFoundException(
                'No product found for id '.$id
                );
            }

            $user->setFullName($fullName);
            $user->setFirstName($firstName);
            $user->setProfession($profession);
            $user->setDepartment($department);
            $user->setCompany($company);
            $user->setSiteUrl($site_url);
            
            if(!empty($password)) {
                $encoded = $encoder->encodePassword($user, $password);
                $user->setPassword($encoded);
            }
            $user->setPublishedAt(new \DateTime());
            $entityManager->flush();

            return $this->redirectToRoute('users_list');
        
    }
}
