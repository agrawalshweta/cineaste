<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Events;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CategoryController extends AbstractController
{
    /**
     * @Route("/admin/category", name="category_list")
     *
     */
    public function index(CategoryRepository $cat)
    {
        $category = $cat->findBy([], ['publishedAt' => 'DESC']);

        return $this->render('admin/category/index.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'category' => $category
        ]);
    }


    /**
     * @Route("/admin/add", name="add_category")
     *
     */
    public function addCat()
    {
        return $this->render('admin/category/add_cat.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'single_cat'=>''
        ]);
    }

    /**
     * @Route("/admin/edit/{id<\d+>}", name="edit_category")
     *
     */
    public function editCat(Category $cat)
    {
        return $this->render('admin/category/edit_cat.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'single_cat'=>$cat
        ]);
    }


    /**
     * @Route("/admin/save_category", name="save_category")
     *
     */
    public function saveCategory(Request $request)
    {
        $input = $request->request->all();

            $validator = Validation::createValidator();

            $constraint = new Assert\Collection([
                '_name'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Name should not be blank.']),
                '_id'    => new \Symfony\Component\Validator\Constraints\Optional(),
                
             ]);

        if ('POST' == $request->getMethod()) {

            $violations = $validator->validate($input, $constraint);

            if (count($violations) == 0) {
                $name = $request->request->get('_name');
                $user = $this->getUser();

                $cat = new Category();

                $cat->setName($name);
                $cat->setAuthor($user);
                $cat->setStatus(1);
                $cat->setPublishedAt(new \DateTime());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($cat);
                $entityManager->flush();
                
                return $this->redirectToRoute('category_list');
            } else {
                foreach ($violations as $error) {
                   
                    $field = substr($error->getPropertyPath(), 1, -1);

                    $errors[$field] = $error->getMessage();
                }
                
                return $this->render('admin/category/add_cat.html.twig', [
                    'last_username' => '',
                    'otherError' => '',
                    'error' => $errors,
                    'single_cat'=>''
                ]);
            }
        }

        return $this->render('admin/category/add_cat.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'single_cat'=>''
        ]);

    }
   
   /**
     * Finds and displays a category entity.
     *
     * @Route("/admin/show/{id<\d+>}", methods={"GET"}, name="cat_show")
     */
    public function show(Category $cat)
    {
        return $this->render('admin/category/show.html.twig', [
            
            'single_cat' => $cat
        ]);
        
    }
    

    /**
     * Update Category.
     *
     * @Route("/admin/update_cat", name="update_cat")
     */
    public function updateCat(Request $request)
    {
            $input = $request->request->all();

            $validator = Validation::createValidator();

            $constraint = new Assert\Collection([
                '_id'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'ID should not be blank.']),
                '_name' => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Name should not be blank.']),
                
             ]);

            $id = $request->request->get('_id');
            $name = $request->request->get('_name'); 

            $entityManager = $this->getDoctrine()->getManager();
            $cat = $entityManager->getRepository(Category::class)->find($id);

           
            if (!$cat) {
                throw $this->createNotFoundException(
                'No product found for id '.$id
                );
            }

            $cat->setName($name);
            $cat->setPublishedAt(new \DateTime());
            $entityManager->flush();

            return $this->redirectToRoute('category_list');
        
    }

    /**
     * Change category status
     *
     * @Route("/admin/change_cat_status/{id}/{status}", name="change_cat_status")
     */
    public function changeCatStatus($id, $status)
    {
            $entityManager = $this->getDoctrine()->getManager();
            $cat = $entityManager->getRepository(Category::class)->find($id);

           
            if (!$cat) {
                throw $this->createNotFoundException(
                'No product found for id '.$id
                );
            }

            $cat->setStatus($status);
            $entityManager->flush();

        return $this->redirectToRoute('category_list');
        
    }
}
