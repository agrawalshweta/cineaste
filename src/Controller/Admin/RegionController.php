<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Events;
use App\Entity\Region;
use App\Repository\RegionRepository;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class RegionController extends AbstractController
{
    /**
     * @Route("/admin/regions", name="regions_list")
     *
     */
    public function index(RegionRepository $reg)
    {
        $regions = $reg->findBy([], ['publishedAt' => 'DESC']);

        return $this->render('admin/regions/index.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'regions' => $regions
        ]);
    }

    /**
     * @Route("/admin/add_region", name="add_region")
     *
     */
    public function addRegion()
    {
        return $this->render('admin/regions/add_region.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'single_region'=>''
        ]);
    }


    /**
     * @Route("/admin/save_region", name="save_region")
     *
     */
    public function saveRegion(Request $request)
    {
        $input = $request->request->all();

            $validator = Validation::createValidator();

            $constraint = new Assert\Collection([
                '_name'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Name should not be blank.']),
                '_id'    => new \Symfony\Component\Validator\Constraints\Optional(),
                
             ]);

        if ('POST' == $request->getMethod()) {

            $violations = $validator->validate($input, $constraint);

            if (count($violations) == 0) {
                $name = $request->request->get('_name');
                $user = $this->getUser();

                $reg = new Region();

                $reg->setName($name);
                $reg->setAuthor($user);
                $reg->setStatus(1);
                $reg->setPublishedAt(new \DateTime());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($reg);
                $entityManager->flush();
                
                return $this->redirectToRoute('regions_list');
            } else {
                foreach ($violations as $error) {
                   
                    $field = substr($error->getPropertyPath(), 1, -1);

                    $errors[$field] = $error->getMessage();
                }
                
                return $this->render('admin/regions/add_region.html.twig', [
                    'last_username' => '',
                    'otherError' => '',
                    'error' => $errors,
                    'single_region'=>''
                ]);
            }
        }

        return $this->render('admin/regions/add_region.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'single_region'=>''
        ]);

    }


    /**
     * @Route("/admin/edit_region/{id<\d+>}", name="edit_region")
     *
     */
    public function editCat(Region $reg)
    {
        return $this->render('admin/regions/edit_region.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            'single_region'=>$reg
        ]);
    }

    /**
     * Update Region.
     *
     * @Route("/admin/update_region", name="update_region")
     */
    public function updateRegion(Request $request)
    {
            $input = $request->request->all();

            $validator = Validation::createValidator();

            $constraint = new Assert\Collection([
                '_id'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'ID should not be blank.']),
                '_name' => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Name should not be blank.']),
                
             ]);

            $id = $request->request->get('_id');
            $name = $request->request->get('_name'); 

            $entityManager = $this->getDoctrine()->getManager();
            $reg = $entityManager->getRepository(Region::class)->find($id);

           
            if (!$reg) {
                throw $this->createNotFoundException(
                'No product found for id '.$id
                );
            }

            $reg->setName($name);
            $reg->setPublishedAt(new \DateTime());
            $entityManager->flush();

            return $this->redirectToRoute('regions_list');
        
    }


    /**
     * Finds and displays a region entity.
     *
     * @Route("/admin/show_region/{id<\d+>}", methods={"GET"}, name="show_region")
     */
    public function show(Region $reg)
    {
        return $this->render('admin/regions/show_region.html.twig', [
            
            'single_region' => $reg
        ]);
        
    }


    /**
     * Change region status
     *
     * @Route("/admin/change_region_status/{id}/{status}", name="change_region_status")
     */
    public function changeRegionStatus($id, $status)
    {
            $entityManager = $this->getDoctrine()->getManager();
            $reg = $entityManager->getRepository(Region::class)->find($id);

           
            if (!$reg) {
                throw $this->createNotFoundException(
                'No product found for id '.$reg
                );
            }

            $reg->setStatus($status);
            $entityManager->flush();

        return $this->redirectToRoute('regions_list');
        
    }
}
