<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class RegistrationController extends AbstractController
{
    /**
     * @Route("/registration", name="registration_index")
     *
     */
    public function index()
    {
        return $this->render('registration/index.html.twig', [
            // last username entered by the user (if any)
            'last_username' => '',
            'otherError' => '',
            // last authentication error (if any)
            'error' => '',
        ]);
    }

    /**
     * @Route("/save_register", name="user_registration")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {

               $input = $request->request->all();

                $validator = Validation::createValidator();

                $constraint = new Assert\Collection([
                    
                    '_username' => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Login Name should not be blank.']),
                    '_email' => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Email should not be blank.']),

                    '_cemail'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Confirm the email address should not be blank.']),

                    '_first_name'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'First name should not be blank.']),
                    '_full_name'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Name should not be blank.']),

                    '_profession'    => new \Symfony\Component\Validator\Constraints\Optional(),
                    '_department'    => new \Symfony\Component\Validator\Constraints\Optional(),
                    '_company'    => new \Symfony\Component\Validator\Constraints\Optional(),
                    '_site_url'    => new \Symfony\Component\Validator\Constraints\Optional(),
                    '_captcha'    => new \Symfony\Component\Validator\Constraints\Optional(),
                    
                ]);

                if ('POST' == $request->getMethod()) {

                    $violations = $validator->validate($input, $constraint);
                    $errors = [];
                    $email = $request->request->get('_email'); 
                    $cemail = $request->request->get('_cemail'); 
                    $trigger = true;
                    if(!empty($email) && !empty($cemail)) {
                        if ($email!==$cemail)
                        {
                            $trigger = false;
                        }
                    }



                    if (count($violations) == 0 && $trigger!=false) {

                        $userName = $request->request->get('_username'); 
                        $email = $request->request->get('_email'); 
                        $firstName = $request->request->get('_first_name');
                        $fullName = $request->request->get('_full_name');
                        $profession = $request->request->get('_profession');
                        $department = $request->request->get('_department');
                        $company = $request->request->get('_company');
                        $site_url = $request->request->get('_site_url');

                        $user = new User();

                        $user->setUsername($userName);
                        $user->setEmail($email);
                        $user->setFullName($fullName);
                        $user->setFirstName($firstName);
                        $user->setProfession($profession);
                        $user->setDepartment($department);
                        $user->setCompany($company);
                        $user->setSiteUrl($site_url);

                        $roles[] = 'ROLE_USER';
                        $user->setRoles($roles);

                        $plainPassword = random_bytes(10);
                        $encoded = $encoder->encodePassword($user, $plainPassword);

                        $user->setPassword($encoded);
                        $user->setPublishedAt(new \DateTime());
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($user);
                        $entityManager->flush();

                        return $this->redirectToRoute('registration_index');
                    } else {
                    
                        
                        foreach ($violations as $error) {
                           
                            $field = substr($error->getPropertyPath(), 1, -1);

                            $errors[$field] = $error->getMessage();
                        }
                        if($trigger==false) {
                            $errors['_email'] = 'Email and confirm email should be same';
                        }
                        
                        return $this->render('registration/index.html.twig', [
                            'last_username' => '',
                            'otherError' => $errors,
                            'error' => '',
                        ]);
                    }


                }
    }

}
