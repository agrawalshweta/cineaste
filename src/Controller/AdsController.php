<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Events;
use App\Entity\Ads;
use App\Repository\AdsRepository;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AdsController extends AbstractController
{
    /**
     * @Route("/ads", name="placeads")
     *
     */
    public function index()
    {
        return $this->render('ads/index.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
            //'ads' => $authorAds
        ]);
    }

   
    /**
     * @Route("/save_ads", name="save_ads")
     *
     */
    public function saveAds(Request $request)
    {
        $input = $request->request->all();

            $validator = Validation::createValidator();

            $constraint = new Assert\Collection([
                '_title'    => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Title should not be blank.']),
                '_content' => new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'Content should not be blank.']),
                
             ]);

        if ('POST' == $request->getMethod()) {

            $violations = $validator->validate($input, $constraint);

            if (count($violations) == 0) {
                $title = $request->request->get('_title');
                $content = $request->request->get('_content'); 
                $user = $this->getUser();

                $ads = new Ads();

                $ads->setTitle($title);
                $ads->setContent($content);
                $ads->setAuthor($user);
                $ads->setStatus(1);
                $ads->setPublishedAt(new \DateTime());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($ads);
                $entityManager->flush();

                return $this->redirectToRoute('placeads');
            } else {
                foreach ($violations as $error) {
                   
                    $field = substr($error->getPropertyPath(), 1, -1);

                    $errors[$field] = $error->getMessage();
                }

                return $this->render('ads/index.html.twig', [
                    'last_username' => '',
                    'otherError' => $errors,
                    'error' => '',
                ]);
            }
        }

        return $this->render('ads/index.html.twig', [
            'last_username' => '',
            'otherError' => '',
            'error' => '',
        ]);
    }


    /**
     * @Route("/ad_list", name="ad_list")
     *
     */
    public function adList(AdsRepository $ads)
    {
        if($this->isGranted('ROLE_ADMIN')) {
           $authorAds = $ads->findBy([], ['publishedAt' => 'DESC']);
        } else {
            $authorAds = $ads->findBy(['author' => $this->getUser()], ['publishedAt' => 'DESC']);
        }


        return $this->render('ads/ads.html.twig', [
            
            'ads' => $authorAds
        ]);
    }

    
    /**
     * Finds and displays a Post entity.
     *
     * @Route("/show/{id<\d+>}", methods={"GET"}, name="ad_show")
     */
    public function show(Ads $ad)
    {
        return $this->render('ads/show.html.twig', [
            
            'single_ad' => $ad
        ]);
        
    }

    /**
     * Change ads status
     *
     * @Route("/change_status/{id}/{status}", name="change_status")
     */
    public function changeStatus($id, $status)
    {
            $entityManager = $this->getDoctrine()->getManager();
            $ads = $entityManager->getRepository(Ads::class)->find($id);

           
            if (!$ads) {
                throw $this->createNotFoundException(
                'No product found for id '.$id
                );
            }

            $ads->setStatus($status);
            $entityManager->flush();

        return $this->redirectToRoute('ad_list');
        
    }

}
